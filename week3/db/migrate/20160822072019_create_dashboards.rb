class CreateDashboards < ActiveRecord::Migration[5.0]
  def change
    create_table :dashboards do |t|

    	t.string :name
    	t.integer :phone
    	t.text :address


      t.timestamps null: false
    end
  end
end
