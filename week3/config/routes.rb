Rails.application.routes.draw do

	root 'dashboards#index'

	resources :dashboards
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.htm

end
